from io import TextIOWrapper
import json
import logging

from flask import request, jsonify, abort

from database.dbuser import DBUser

from protocols.alexaaction import g_dispatch
from twisted.web import server
from twisted.web.resource import Resource
from twisted.python import log

class AlexaIncoming(Resource):

    isLeaf = True

    def render_GET(self, request):

        response = create_response("I am sorry, but I did not understand that")
        response = json.dumps(response, ensure_ascii=False).encode('utf-8')
        request.responseHeaders.addRawHeader(b"content-type", b"application/json")

        return response


    def render_POST(self, request):

        def send_response(response):

            r = jsonresponse(request, create_response(response["PlainText"], endsession=response["EndSession"]))
            request.write(r)
            request.finish()

        payload = json.loads(request.content.getvalue().decode("utf-8"))

        print(json.dumps(payload))

        # log.msg(payload)

        u = DBUser().get(user_token=payload["session"]["user"]["accessToken"])

        #u = {
        #    "scapp_token": "jhgwjkgkwerhgt"
        #}

        if u:

            scapp_token = u["scapp_token"]

            if "intent" in payload["request"]:

                if scapp_token in g_dispatch:
                    w = g_dispatch[scapp_token]

                    pl = {
                        "sessionId": payload["session"]["sessionId"],
                        "intent": payload["request"]["intent"]
                    }

                    print("sending", json.dumps(pl))

                    w.sendJson(pl, send_response)

                    return server.NOT_DONE_YET

        return jsonresponse(request, create_response("I am sorry, but I could not reach your starcounter system. Is it up and running?"))


def jsonresponse(r, response):

    response = json.dumps(response, ensure_ascii=False).encode('utf-8')
    r.responseHeaders.addRawHeader(b"content-type", b"application/json")

    return response


def create_response(message, type="PlainText", endsession=True):
    return {
        "version": "1.0",
        "response": {
            "outputSpeech": {
                "type": type,
                "text": message if type=="PlainText" else None,
                "ssml": message if type=="SSML" else None
            },
            "shouldEndSession": endsession
        }
    }


def incoming():

    payload = request.json

    # lookup user from access_token

    # u = DBUser().get(user_token=payload["session"]["user"]["accessToken"])
    u = {
        "scapp_token": "jhgwjkgkwerhgt"
    }

    if u:

        scapp_token = u["scapp_token"]

        if scapp_token in g_dispatch:

            w = g_dispatch[scapp_token]

            try:

                """
                w.sendJson({
                    "Provider": "Google",
                    "ChannelId": channel,
                    "Message": state
                })
                """


            except:
                logging.exception("Kataschmack")

        else:
            logging.info("%s no one's listening ... :-(" % scapp_token)


    return jsonify(create_response("I am sorry, but I can not get through to Your Starcounter system. Is it up and running?"))



