__author__ = 'Johan Olofsson (johano99@gmail.com)'

import re
import pytz
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import date, datetime, timedelta

def parse_date_to_utc(dt_s):

    dt = parser.parse(dt_s)
    return ensure_utc(dt)

def ensure_utc(dt):

    if not dt.tzinfo:
        dt = dt.replace(tzinfo=pytz.utc)

    else:
        dt = dt.astimezone(pytz.utc)

    return dt


def daterange(start_date, end_date, step="days", stepval=1):
    if step == "days":
        for n in range(int((end_date.date() - start_date.date()).days)+1):
            yield datetime.combine(start_date.date(),datetime.min.time()) + timedelta(n)
    if step == "hours":
        for n in range((int((end_date - start_date).total_seconds()/60/60))/stepval+1):
            yield start_date + timedelta(hours=n*stepval)
    if step == "minutes":
        for n in range((int((end_date - start_date).total_seconds()/60))/stepval+1):
            yield start_date + timedelta(minutes=n*stepval)


def datetime_tostring(dt):

    return dt.replace(tzinfo=pytz.utc, microsecond=0).isoformat()

def datetime_tostring_notz(dt):

    return dt.replace(tzinfo=None, microsecond=0).isoformat()


def is_too_long_ago(last_updated, **kwargs):

    return not last_updated or datetime.utcnow()-last_updated > timedelta(**kwargs)


def parse_relativedelta(s):

    """
    years=1 months=1 weeks=1 days=7 hours=1 minutes=1 seconds=1
    """
    if s is None:
        return relativedelta()

    d = re.findall(r'(years|months|days|hours|minutes|seconds)\s*=\s*(\d+)', str(s))
    kwargs = dict((key, int(value)) for key,value in d)

    return relativedelta(**kwargs)


def main():

    rd = parse_relativedelta("months=1")

    dt = datetime(2016,1,30)+rd

    pass

if __name__ == "__main__":
    main()
