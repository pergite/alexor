import json
import logging
from twisted.internet.protocol import Protocol, Factory, connectionDone
from protocols.jsonsocketprotocol import JSONSocketProtocol

g_dispatch = {}

class AlexaAction(JSONSocketProtocol):

    def __init__(self):

        self.remote=None
        self.seqno = 1
        self.app_token = None
        self.callbacks={}

        super(AlexaAction, self).__init__()

    def connectionMade(self):

        self.remote=self.transport.getPeer()
        logging.info("Connect: %s" % self.remote)

    def connectionLost(self, reason=connectionDone):

        logging.info("Disconnect: %s" % self.remote)

        # TODO: need a lock on g_dispatch
        if self.app_token in g_dispatch:
            del g_dispatch[self.app_token]

    def jsonReceived(self, data):

        logging.info("jsonReceived %s" % json.dumps(data))

        if data["Type"] == "Register":

            self.app_token = data["AppToken"]

            # TODO: need a lock on g_dispatch
            g_dispatch[self.app_token] = self

        elif data["Type"] == "Response":

            seqno = data["SeqNo"]
            self.callbacks[seqno](data["Data"])
            del self.callbacks[seqno]

    def sendJson(self, data, response_callback):

        self.seqno += 1
        seqno = self.seqno

        self.callbacks[str(seqno)] = response_callback

        payload={
            "SeqNo": str(seqno),
            "Data": data
        }

        super().sendJson(payload)


class AlexaActionFactory(Factory):
    def buildProtocol(self, addr):
        return AlexaAction()
