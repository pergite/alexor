import os

ALEXA_ACTION_PORT = 9997
HTTP_SERVER_PORT = 9996


BASE_URL = "https://alexor.pergite.com"
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DEBUG = False

SERVER_NAME = "alexor.pergite.com"

MONGO_CONNECTION = "mongodb://127.0.0.1:27017"
MONGO_DATABASE = "alexor"

MEMCACHED_SERVERS = ["127.0.0.1"]

try:
    from settings_local import *

except ImportError:
    pass
