import os, sys
import logging
from webapp import webapp, rest

from autobahn.twisted.resource import WSGIRootResource
from twisted.python import log
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.web.server import Site
from twisted.web.wsgi import WSGIResource
from twisted.web.static import File, Data
from klein import Klein

from views.index import index
from views.link import link
#from alexa.views import incoming

from protocols.alexaaction import AlexaActionFactory

import settings

from alexa.views import AlexaIncoming

logging.basicConfig(stream=sys.stderr, format='%(asctime)s %(message)s', level=logging.INFO)

# last entry will match *any* path and pass it onto angular-routing
#webapp.add_url_rule("/incoming", "/incoming", incoming)
webapp.add_url_rule("/link", "/link", link, methods=["GET", "POST"])
webapp.add_url_rule("/", "/index", index)
webapp.add_url_rule("/<path:path>", "/index", index)


if __name__ == "__main__":

    log.startLogging(sys.stdout)

    endpoint = TCP4ServerEndpoint(reactor, settings.ALEXA_ACTION_PORT)
    endpoint.listen(AlexaActionFactory())

    wsgiResource = WSGIResource(reactor, reactor.getThreadPool(), webapp)

    root = WSGIRootResource(wsgiResource, {
        b"alexa": AlexaIncoming(),
        b"static": File(os.path.join(os.path.dirname(__file__), 'static'))
    })

    site = Site(root)

    reactor.listenTCP(settings.HTTP_SERVER_PORT, site)
    reactor.run()
