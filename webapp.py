__author__ = 'johano'
import logging
from flask import Flask, redirect, url_for, session, request, render_template, abort

from flask_mongoengine import MongoEngine

from flask_login import LoginManager, current_user
from flask_restful import Api
from jinja2 import evalcontextfilter, Markup
from datetime import datetime

from hosting.reverseproxied import ReverseProxied
from hosting.customjson import CustomJSONEncoder, custom_json_output
from hosting.regexconverter import RegexConverter

from version import get_version

from settings import *

webapp = Flask(__name__)

db = MongoEngine()
db.init_app(webapp)

rest = Api(webapp)
rest.representations.update({
    'application/json': custom_json_output
})


@webapp.context_processor
def inject_globals():
    return {
        "version": get_version()
    }


@webapp.template_filter()
@evalcontextfilter
def nl2br(eval_ctx, value):
    return Markup(value.replace('\n', '<br>'))


@webapp.errorhandler(401)
def access_denied(e):

    message="""The Page that you seek
Is hidden from view, and so
Permission denied."""

    status = 401
    return render_template('customerror.html', message=message, status=status), status


webapp.url_map.converters['regex'] = RegexConverter
webapp.wsgi_app = ReverseProxied(webapp.wsgi_app)
webapp.json_encoder = CustomJSONEncoder

webapp.secret_key = "production"

webapp.config["MONGODB_SETTINGS"] = {'DB': MONGO_DATABASE}
webapp.config["ASSETS_DEBUG"] = DEBUG

login_manager = LoginManager()
login_manager.init_app(webapp)

from functools import wraps
from flask import g, Response

def rest_login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user is None or current_user.is_anonymous:
            return Response("401 ACCESS DENIED", 401)

        return f(*args, **kwargs)
    return decorated_function

def rest_nocache():

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            data = f(*args,**kwargs)

            nocache = {
                "Last-Modified": datetime.now(),
                "Cache-Control": "no-store, no-cache, must-revalidate, max-age=0",
                "Pragma": "no-cache",
                "Expires": "-1"
            }

            return data, 200, nocache

        return wrapper

    return decorator

def restful_exceptions():

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args,**kwargs)

            except Exception as ex:
                logging.exception("Boom")
                return ex.message, 500

        return wrapper

    return decorator


def login_required(**kwargs):

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwinnerargs):

            if current_user and not current_user.is_anonymous:

                required_roles = None
                allowed_roles = None

                if "required_roles" in kwargs:

                    required_roles = kwargs["required_roles"]
                    if not isinstance(required_roles, list):
                        required_roles = [required_roles]

                if "allowed_roles" in kwargs:

                    allowed_roles = kwargs["allowed_roles"]
                    if not isinstance(allowed_roles, list):
                        allowed_roles = [allowed_roles]

                check = True

                if required_roles:
                    check = all([required_role in current_user["roles"] for required_role in required_roles])

                if check and allowed_roles:
                    check = any([allowed_role in current_user["roles"] for allowed_role in allowed_roles])

                if check:
                    return f(*args, **kwinnerargs)

            if "redirect_to_login" in kwargs and kwargs["redirect_to_login"]:
                return login_manager.unauthorized()

            return abort(401)

        return wrapper

    return decorator


# TODO: consider moving LoginManager stuff to separate file
@login_manager.unauthorized_handler
def unauthorized_callback():

    url = "/login?next=" + request.path
    if request.query_string:
        url += "?" + request.query_string

    return redirect(url)

