__author__ = 'johano'

import uuid

from datetime import datetime, timedelta

from flask import render_template, redirect, request, abort, session, request, jsonify
from flask_login import current_user, logout_user
from webapp import login_required

from database.dbuser import DBUser

import settings


def index(path=None):

    cu = {
    }

    return render_template("index.html", settings=settings, current_user=cu)

