__author__ = 'johano'

import uuid

from datetime import datetime, timedelta

from flask import render_template, redirect, request, abort, session, request, jsonify
from flask_login import current_user, logout_user
from webapp import login_required

from database.dbuser import DBUser

import settings

def link():

    if request.method=="POST":

        payload = request.json
        access_token = str(uuid.uuid4()).replace("-", "")

        u = {
            "user_token": access_token,
            "scapp_token": payload["token"]
        }

        DBUser().save(u)

        return jsonify({
            "status": True,
            "userToken": access_token
        })

    else:

        alexa_connect = {
            "state": request.args.get("state"),
            "redirect_uri": request.args.get("redirect_uri")
        }

        return render_template("link.html", settings=settings, alexa_connect=alexa_connect)
