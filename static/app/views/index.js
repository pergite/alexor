angular.module('alexor.views.index', ['ui.router','alexor.constants'])
.config(function($stateProvider,$site) {

    $stateProvider
    .state('alexor', {
      url: "/",
      ncyBreadcrumb: {label: 'Home'},
      templateUrl: "/static/app/views/index.html?"+$site.version,
      controller:'indexCtrl'

    })
})
.controller("indexCtrl",
    function($scope, $http, $site, $currentUser, $state) {

    }
)
;