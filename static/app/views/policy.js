angular.module('alexor.views.policy', ['ui.router','alexor.constants'])
.config(function($stateProvider,$site) {

    $stateProvider
    .state('alexor.policy', {
      url: "/policy",
      ncyBreadcrumb: {label: 'Policy'},
      templateUrl: "/static/app/views/policy.html?"+$site.version,
      controller:'policyCtrl'

    })
})
.controller("policyCtrl",
    function($scope, $http, $site, $currentUser, $state) {

    }
)
;