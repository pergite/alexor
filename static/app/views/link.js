angular.module('alexor.views.link', ['ui.router','alexor.constants'])
.config(function($stateProvider,$site) {

    $stateProvider
    .state('link', {
      url: "/link",
      ncyBreadcrumb: {label: 'Home'},
      templateUrl: "/static/app/views/link.html?"+$site.version,
      controller:'linkCtrl'

    })
})
.controller("linkCtrl",
    function($scope, $http, $site, $state, $alexaConnect) {

        $scope.scapp_token = '';
        $scope.link = function() {

            var p = {
                token: $scope.scapp_token
            }

            $http.post("/link", p).then(function(result) {
                document.location.href = $alexaConnect.redirect_uri + "#state=" + $alexaConnect.state + "&access_token=" + result.data.userToken + "&token_type=Bearer";
            });
        }
    }
)
;