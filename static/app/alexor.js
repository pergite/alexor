angular.module("alexor", [
    "ui.router",
    "ui.bootstrap",
    "ui.bootstrap.datetimepicker",
    "ngJsTree",
    "mgcrea.ngStrap",
    'ngSanitize',
    "ui.select",
    "ngAnimate",
    "angularMoment",
    "ncy-angular-breadcrumb",
    "ngSanitize",
    "LocalStorageModule",
    "ui.sortable",
    "cp.ngConfirm",
    "alexor.views.index",
    "alexor.views.link",
    "alexor.views.policy",
])
.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise("/");
})
.filter('unsafe', function($sce) {
    return $sce.trustAsHtml;
})
.filter('offset', function() {
  return function(input, start) {
    if(!input)
        return input;
    start = parseInt(start, 10);
    return input.slice(start);
  };
})
.filter('strLimit', function($filter) {
   return function(input, limit) {
      if (!input)
        return;

      if (input.length <= limit) {
          return input;
      }

      return $filter('limitTo')(input, limit) + '...';
   };
})
;
