__author__ = 'johano'

# no need to bump this up manually - its done automatically (in the version_local.py)
# when your doing a  "git push web"

VERSION_NUMBER="0001"

def get_version():
    return VERSION_NUMBER

try:
    from version_local import *

except ImportError:
    pass
