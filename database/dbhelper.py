__author__ = 'johano'

from bson.objectid import ObjectId

EXCEPTIONS = []


def ensure_objectids(o, idname=True):
    if isinstance(o, list):

        for i in range(0, len(o)):
            v = o[i]

            if isinstance(v, dict):
                o[i] = ensure_objectids(v)

            elif idname:
                o[i] = ObjectId(v) if not isinstance(v, ObjectId) else v

        return o

    elif isinstance(o, dict):

        for k, v in o.items():

            if isinstance(v, dict):
                o[k] = ensure_objectids(o[k])

            elif isinstance(v, list):

                o[k] = ensure_objectids(o[k], k.endswith("_id"))

            elif k.endswith("_id") and k not in EXCEPTIONS and v:

                if isinstance(v, list):

                    for i in range(0, len(v)):
                        vi = v[i]
                        try:
                            v[i] = ObjectId(vi) if not isinstance(vi, ObjectId) else vi
                        except:
                            pass  # guld
                else:
                    try:
                        o[k] = ObjectId(v)

                    except:
                        pass  # gulp

        return o

    return ObjectId(o) if not isinstance(o, ObjectId) else o


def get_update_object(**kwargs):
    u = {}

    for k in ("update_set", "update_unset", "update_push", "update_pull", "update_addtoset", "set_on_insert"):

        if k not in kwargs:
            continue

        v = ensure_objectids(kwargs[k])

        if k == "update_set":
            u.update({"$set": v})

        if k == "update_unset":
            u.update({"$unset": v})

        if k == "update_push":
            u.update({"$push": v})

        if k == "update_pull":
            u.update({"$pull": v})

        if k == "update_addtoset":
            u.update({"$addToSet": v})

        if k == "set_on_insert":
            u.update({"$setOnInsert": v})

    return u
