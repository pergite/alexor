
__author__ = 'johano'

from database.mongoclient import MongoClientConnector
from database.dbhelper import ensure_objectids, get_update_object
from datetime import datetime

import pymongo

class DBBase(object):

    def __init__(self, collection):
        self.collection = collection


    def get_db(self):

        return MongoClientConnector.get_db()

    def get(self, **kwargs):

        db = MongoClientConnector.get_db()

        query=ensure_objectids(kwargs)

        return db.get_collection(self.collection).find_one(query)


    def find(self, **kwargs):

        db = MongoClientConnector.get_db()

        query=ensure_objectids(kwargs)

        return db.get_collection(self.collection).find(query)


    def save(self, o):

        db = MongoClientConnector.get_db()

        ensure_objectids(o)

        if "created" in o:
            del o["created"]

        if "_id" in o:
            o = db.get_collection(self.collection).find_and_modify({"_id": o["_id"]}, update={"$set": o}, new=True, upsert=True)

        else:
            o["created"] = datetime.utcnow()
            db.get_collection(self.collection).save(o)

        return o

    def raw_update(self, q, u, upsert=False, multi=False):

        q=ensure_objectids(q)
        u=ensure_objectids(u)

        db = MongoClientConnector.get_db()
        #db.get_collection(self.collection).update(q, u, upsert=upsert,multi=multi)

        o = db.get_collection(self.collection).find_and_modify(q, update=u, new=True, upsert=upsert)

        return o

    def update(self, q, **kwargs ):

        u = get_update_object(**kwargs)
        q = ensure_objectids(q)

        db = MongoClientConnector.get_db()

        if u:
            o = db.get_collection(self.collection).find_and_modify(q, update=u,new=True,upsert=False)

        else:
            o = db.get_collection(self.collection).find_one(q)

        return o

    def upsert(self, q, **kwargs ):

        u = get_update_object(**kwargs)
        q = ensure_objectids(q)

        db = MongoClientConnector.get_db()

        if u:
            o = db.get_collection(self.collection).find_and_modify(q, update=u,new=True,upsert=True)

        else:
            o = db.get_collection(self.collection).find_one(q)

        return o

    def update_many(self, q, **kwargs):

        db = MongoClientConnector.get_db()

        q = ensure_objectids(q)
        u = get_update_object(**kwargs)

        db.get_collection(self.collection).update(q, u, upsert=False,multi=True)


    def delete(self, multi=False, **kwargs):

        db = MongoClientConnector.get_db()

        query=ensure_objectids(kwargs)

        db.get_collection(self.collection).remove(query, multi=multi)
