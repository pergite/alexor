__author__ = 'Johan Olofsson (johano99@gmail.com)'

import threading
from pymongo import MongoClient
from settings import MONGO_CONNECTION, MONGO_DATABASE

thread_local = threading.local()


class MongoClientConnector:

    @staticmethod
    def get_client():
        client = getattr(thread_local, 'mongo_client', None)

        if client is None:
            client = MongoClient(MONGO_CONNECTION)
            setattr(thread_local, 'mongo_client', client)

        return client

    @staticmethod
    def get_db():
        db = getattr(thread_local, 'mongo_client_db', None)

        if db is None:
            db = MongoClientConnector.get_client()[MONGO_DATABASE]
            setattr(thread_local, 'mongo_client_db', db)

        return db

    def __init__(self):
        pass


