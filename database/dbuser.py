from database.dbbase import DBBase


class DBUser(DBBase):

    def __init__(self):
        super().__init__("users")
