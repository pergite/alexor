__author__ = 'Johan Olofsson (johano99@gmail.com)'

import json

from flask import make_response
from datetime import datetime
from helper.datestuff import datetime_tostring
from bson.objectid import ObjectId

class CustomJSONEncoder(json.JSONEncoder):

    def default(self, obj):

        if isinstance(obj, datetime):
            return datetime_tostring(obj)

        if isinstance(obj, ObjectId):
            return str(obj)

        return json.JSONEncoder.default(self, obj)

def custom_dumps(o):
    return json.dumps(o, cls=CustomJSONEncoder)

def custom_loads(s):
    return json.loads(s)

def custom_json_output(data, code, headers=None):
    dumped = json.dumps(data, cls=CustomJSONEncoder)
    resp = make_response(dumped, code)
    resp.headers.extend(headers or {})
    return resp
